# An Introduction To PGP

**Pretty Good Privacy (PGP)** is an encryption program that provides cryptographic privacy and authentication for data communication. PGP is used for signing, encrypting, and decrypting texts, e-mails, files, directories, and whole disk partitions and to increase the security of e-mail communications. PGP was first introduced by Phil Zimmermann in 1991. PGP encryption uses a combination of hashing, data compression, symmetric-key cryptography, and finally public-key cryptography. Each step uses one of several supported algorithms. Current versions of PGP encryption include options through an automated key management server.

## The Web Of Trust

This is a feature of PGP styled encryption which to me makes it unique over other types of encryption. The Web Of Trust is as follows described by Zimmerman.

**As time goes on, you will accumulate keys from other people that you may want to designate as trusted introducers. Everyone else will each choose their own trusted introducers. And everyone will gradually accumulate and distribute with their key a collection of certifying signatures from other people, with the expectation that anyone receiving it will trust at least one or two of the signatures. This will cause the emergence of a decentralized fault-tolerant web of confidence for all public keys.**

While not widely used by PGP users, I urge you to use the WOT system implemented in PGP, as this allows users to be decentralized, peer checked, and disrupts bad-actors.

## Components Of PGP

PGP is simplified and broke down into 4 sections which when added together makes it a very secure system. The Public Key, The Private Key, The Public PGP Fingerprint, and Trust Signatures.

* The Public Key - When generating a PGP key you are given a Public Key and a Private Key. Your Public Key is what you would publish and show others. This key can be then copied and pasted to be used by others to send secure messages to you.

* The Private Key - This key is what you would use to decrypt the messages sent to you by others using your public key. This is your personal key and should not be shown to anyone. You would want to make a copy of this key and put it in a secure place.

* The Public PGP Fingerprint - A public key fingerprint is a shorter version of a public key. From a fingerprint, someone can validate the correct corresponding public key. A fingerprint like C3A6 5E46 7B54 77DF 3C4C 9790 4D22 B3CA 5B32 FF66 can be printed on a business card.

## Trust Signatures

This one is a bit more complex to explain but goes back to the Web Of Trust mentioned previously. Both when encrypting messages and when verifying signatures, it is critical that the public key used to send messages to someone or some entity actually does 'belong' to the intended recipient. Simply downloading a public key from somewhere is not a reliable assurance of that association; deliberate (or accidental) impersonation is possible. Users must also ensure by some means that the public key in a certificate actually does belong to the person or entity claiming it. A given public key may be digitally signed by a third party user to attest to the association between someone and the key. There are several levels of confidence which can be included in such signatures. This allows for Peer to Peer trust. Similar to if You were meeting a friend of a friend, your friend can give verify the trust of the new member.


## GnuPG or GPG

**gpg**  is  the OpenPGP part of the GNU Privacy Guard (GnuPG). It is a tool to provide digital encryption and  signing  services using  the OpenPGP standard. gpg features complete key management and all the bells and whistles you would  expect  from a full OpenPGP implementation.
There are two main versions of GnuPG: GnuPG 1.x and GnuPG 2.x. 

GnuPG 2.x  supports  modern  encryption  algorithms  and  thus should  be  preferred  over  GnuPG  1.x.  You only need to use GnuPG 1.x if your platform doesn't support GnuPG 2.x,  or  you need  support for some features that GnuPG 2.x has deprecated, e.g., decrypting data created with PGP-2 keys.

**Use a good password** for your user account and make  sure  that all  security  issues  are always fixed on your machine.  Also employ diligent physical protection to your machine.  Consider to  use  a good passphrase as a last resort protection to your secret key in the case your machine gets stolen.  It is important  that  your secret key is never leaked.  Using an easy to carry around token or smartcard with the secret key is often advisable.

**Check `man gpg` in your machine.**

## TL;DR GPG Usage

 - Sign doc.txt without encryption (writes output to doc.txt.asc):
   gpg --clearsign {{doc.txt}}

 - Encrypt doc.txt for alice@example.com (output to doc.txt.gpg):
   gpg --encrypt --recipient {{alice@example.com}} {{doc.txt}}

 - Encrypt doc.txt with only a passphrase (output to doc.txt.gpg):
   gpg --symmetric {{doc.txt}}

 - Decrypt doc.txt.gpg (output to stdout):
   gpg --decrypt {{doc.txt.gpg}}

 - Import a public key:
   gpg --import {{public.gpg}}

 - Export public key for alice@example.com (output to stdout):
   gpg --export --armor {{alice@example.com}}

 - Export private key for alice@example.com (output to stdout):
   gpg --export-secret-keys --armor {{alice@example.com}}

### This is a recent tutorial I watched not long ago.

 - [Do This Before Putting Your Files in the Cloud - Mental Outlaw](https://www.youtube.com/watch?v=M0O7vhvQW30)
