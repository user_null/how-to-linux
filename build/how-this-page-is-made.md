## How dumpnull.xyz is made?
# Without any VPS.

Yes. This page was made without using any VPS.

Using [Vercel](https://vercel.com/) and Gitlab only.

### Why?

I was looking out a cheap way to deliver a web page, I alredy had a domain and gitlabs/github pages wasn't an option, because I don't want that dirty .gitlab.io in MY page. So, I was needing some way to show static pages at my domain.
Vercel comes as the first f\* search result, so why even bothering.

### How is managed?

Pretty easy, I just write some .md files in [this](https://gitlab.com/user_null/how-to-linux) repo and "compile" with lowdown the .md into .html with a pre-writted head for the html, after that I just copy the new html into [this](https://gitlab.com/user_null/boringpage)(locally) and just make a commit and pull to gitlab.

### Benefits?

- Free
- Vercel handles SSL certification and secures the website, also they don't inject any spooky JavaScript or something, which is good.
- I have a repo with full commit history remotely, so I can just clone the repo and start typing, I can download the repo and make my own with gitea, migration would be pretty easy.

### Cons?

- I haven't my own :
	* Email client.
	* Cloud Storage.
	* Searx instance.
	* Minecraft Server.
	* Generic federated service instance.
	* VPN service.
	* Very highly illegal streaming site.

#### I want all that?  Yes.


