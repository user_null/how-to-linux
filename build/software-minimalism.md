# Software Minimalism

This is a quick description of Software Minimalism and philosophy as well as the overarching ideals of the philosophies that branch off from it. Software Minimalism is a philosophy inherent to many GNU/Linux distributions and other Unix like operating systems, a common talking point among software enthusiasts. Software Minimalism encompasses the Unix Philosophy. You can read more here as I will be going over most if not all mentioned in the following...

* Unix Philosophy
* Software Minimalism
* Suckless

* Unix Philosophy
* Justification For Minimalism
* Minimal Operating Systems
* Window Managers
* Full Featured Window Managers
* Minimal Window Managers

---

## Unix Philosophy

The Unix philosophy, originated by Ken Thompson, is a set of cultural norms and philosophical approaches to minimalist, modular software development. It is based on the experience of leading developers of the Unix operating system. Early Unix developers brought their main focus to software, which was to be modular and reusable. This spawned the "software tools" movement. Over time, the leading developers of Unix established a set of cultural norms for developing software, these norms became as important and influential as the technology of Unix itself, this has been termed the Unix philosophy.

The Unix Philosophy is often summarized as...

* Write programs that do one thing and do it well.
* Write programs to work together.
* Write programs to handle text streams, because that is a universal interface.

These both lead us the to the Suckless Software Project, suckless.org is a community of programmers working on minimalist free software projects with a focus on simplicity, clarity, and frugality. The group developed dwm and wmii window managers, surf, tabbed, and other programs that are said to adhere strictly to the UNIX philosophy of "doing one thing and doing it well." The suckless community was founded by Anselm R. Garbe from Germany in 2002. He became a vocal proponent of the suckless philosophy, saying that "a lot went wrong in the IT industry recently ... be recognized in order to rethink the common practice, and perhaps to think about the time when Moores law stops being a valid assumption." The suckless manifesto deplores the common tendency for "complex, error prone and slow software that seems to be prevalent in the present day software industry", and argues that a programmers performance should not be measured by the number of lines of code he writes. The development team follows the New Jersey style of "Worse is better" and adheres to the KISS principle, Keep It Simple Stupid.

---

## Justification For Minimalism

By Following the Unix Philosophy we can...

* Make code that is easier to write and maintain
* Facilitate easy scripting and automation
* Decrease bugs in a code base
* Decrease vulnerabilities and remove attack vectors in our software
* Become overall more efficient

A system cannot be minimal if it uses Poettering made software or any free-desktop maintained software, this includes...

* Pulse Audio, Systemd, dbus, Wayland
* Gnome and GTK
* KDE
* GUI based tools
* Programs written in interpreted languages
* Programs written to use XML
* Any of the GNU tools such as the coreutils
* It is listed at https://suckless.org/sucks/ or as harmful under https://harmful.cat-v.org/software/

---

## Minimal Operating Systems

While most modern GNU/Linux Distributions begin to stray from the Unix Philosophy, there are few that adhere to it more so than others. Though this section could be rendered as irrelevant for those who are knowledgeable in GNU/Linux and Unix like operating systems, all can customized and changed to meet the Suckless Software Minimalism standards.

* 9Front
* Open BSD
* Void Linux
* Crux
* Gentoo
* Funtoo
* Alpine Linux
* LFS
* [List of non-systemd distributions](https://sysdfree.wordpress.com/2019/10/12/135/)

## Window Managers

Currently most GNU/Linux and BSD distributions use the X Window System for drawing the desktop, while the X System does not manage the windows, instead it depends on a special client application called a window manager. The window manager moves and sizes windows in response to user input. Window managers can be divided into these three categories...

* Stacking - They allow windows to draw their contents one on top of another on the desktop, starting with the one on the bottom and going up in the "Z order".

* Compositing - Provide a buffer for each window to draw on and then compose those buffers together creating the desktop image. This type of window manager allows use of semitransparent windows.

* Tiling - The windows do not overlap. Tiling Window Managers can also be divided into...
	*   Static tiling WMs such as Ratpoison always use a set number of equal size tiles, and the tiles do not move
	*   Dynamic tiling WMs such as Awesome allow you to change the layout of the tiles, the number of tiles onscreen, and other things.

## Full Featured Window Managers
### Full Dynamic Window Managers

* Awesome
* i3

### Full Stacking Window Managers

* Blackbox
* Fluxbox
* Openbox


## Minimal Window Managers
### Minimal Dynamic Window Managers

* DWM
* Monsterwm

### Minimal Tiling Window Managers

* Bspwm
* Ratpoison
* WMFS2
* Xmonad

### Minimal Stacking Window Managers

* 2bwm
* Rio
* Windowmaker

---

## Other Software

Is worth noticing well made software like text editor and terminal emulators.

### Terminal emulators

* st (suckless terminal)

_B-but st is shit comes with nothing, I need my mom_

I encourage you to download the source code, patch it yourself like a grown-up men and upload it to your own gitea instance, in your own vps, that you like a full men, have set up from scratch, or use [this](https://github.com/siduck76/st)

### Text Editor

* vim/nvim
* micro
* vi
* ved
* ed
* acme
* sam

_B-but I'm a developer_

Oh, of course, use _nvim_


---

Any configuration of Operating System and Window Manager would suffice as long as used with Suckless or similar software. While Software Minimalism can be an extreme approach to computing, it can be extremely helpful in learning about and appreciating GNU/Linux. I would implore you do take some time and read more about the previous software and test out some the Linux and BSD distributions to see if you find any you fancy. Remember however that GNULinux is fun to use and a exploratory process. Do not worry about abiding by this philosophy if your personal needs are not met.
