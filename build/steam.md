# Install Steam in Artix

1. Open `/etc/pacman.conf` with your favorite text editor and root privileges.
2. Uncomment

        [lib32]

        include = /etc/pacman.d/mirrorlist
 
  and

        [multilib]

        Include = /etc/pacman.d/mirrorlist-arch
3. After that, `sudo pacman -Syu steam`
